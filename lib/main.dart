import 'package:flutter/material.dart';
import 'package:tudemo/cart/cart.dart';
import 'package:tudemo/home/home.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return 
    MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
     initialRoute: "/home",
      routes: {
        "/home":(context)=>Home(),
        "/cart" :(context)=>Cart()

      },
    );
  }
}




