import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tudemo/home/blog_bloc/bloc_bloc.dart';
import 'package:tudemo/home/home_page.dart';
import 'package:tudemo/repository/blog_repository.dart';

class Home extends StatelessWidget {
  final BlogRepository _blogRepository = BlogRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BlocBloc>(
      create: (context) => BlocBloc(blogRepository: _blogRepository)
                            ..add(LoadBlog()),
      child: Scaffold(
        appBar: AppBar(title: Text("Cart"),centerTitle: true,),
        body: HomePage(),
      )
    );
  }
}