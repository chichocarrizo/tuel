import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tudemo/cart/cart.dart';
import 'package:tudemo/home/blog_bloc/bloc_bloc.dart';
import 'package:tudemo/models/products.dart';
import 'package:tudemo/repository/blog_repository.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Products> postList = [];
  int cantidad = 0;
  int cart = 0;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocBloc, BlocState>(builder: (context, state) {
      if (state is BlogLoading) {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
      if (state is BlogNoLoaded) {
        return Center(
          child: Column(
            children: <Widget>[Icon(Icons.error), Text('Cannot load posts')],
          ),
        );
      }
      if (state is BlogLoaded) {
        postList = state.posts;
        cart = state.cart;
        return Scaffold(
          body: postList.length == 0
              ? Center(
                  child: Text('No Blog Avaliable'),
                )
              : ListView.builder(
                  itemCount: postList.length,
                  itemBuilder: (_, index) {
                    return Ui(
                      products: postList[index],
                    );
                  },
                ),
          floatingActionButton: Visibility(
              visible: cart == 0 ? false : true,
              child: Align(
                alignment: Alignment.bottomRight,
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 130,
                      height: 130,
                    ),
                    Positioned(
                      height: 200,
                      width: 200,
                      child: Container(
                        width: 100,
                        height: 100,
                        child: IconButton(
                          icon: Icon(Icons.shopping_cart_sharp),
                          onPressed: () {
                            
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Cart()),
                            );
                          },
                          iconSize: 45,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 40,
                      left: 55,
                      child: ClipOval(
                        child: Container(
                          width: 35,
                          height: 35,
                          color: Colors.red[300],
                          child: Align(
                            child: Text(
                              cart.toString(),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )),
        );
      }
      return Container();
    });
  }
}

class Ui extends StatefulWidget {
  final Products products;

  const Ui({this.products});

  @override
  _UiState createState() => _UiState();
}

class _UiState extends State<Ui> {
  int cantidad = 1;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: postsUI(widget.products.nombre, widget.products.descripcion,
          widget.products.sku, widget.products.id.toString()),
    );
  }

  Widget postsUI(String nombre, String description, String sku, String id) {
    return Card(
      elevation: 10.0,
      margin: EdgeInsets.all(14.0),
      child: Container(
        padding: EdgeInsets.all(14.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  nombre,
                  style: Theme.of(context).textTheme.subtitle2,
                  textAlign: TextAlign.center,
                ),
                Text(
                  sku,
                  style: Theme.of(context).textTheme.subtitle2,
                  textAlign: TextAlign.center,
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              description,
              style: Theme.of(context).textTheme.subtitle1,
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    IconButton(
                        icon: Icon(Icons.remove),
                        onPressed: () {
                          setState(() {
                            if (cantidad == 1) {
                              cantidad = 1;
                            } else {
                              cantidad--;
                            }
                          });
                        }),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      cantidad.toString(),
                      style: Theme.of(context).textTheme.subtitle1,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          setState(() {
                            cantidad++;
                          });
                        }),
                  ],
                ),
                IconButton(
                    icon: Icon(Icons.add_shopping_cart),
                    onPressed: () {
                      cantidad = 1;
                      BlocBloc(blogRepository: BlogRepository())
                        ..add(AddProductCar(int.parse(id), cantidad));
                    })
              ],
            ),
          ],
        ),
      ),
    );
  }
}
