import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tudemo/models/carts.dart';
import 'package:tudemo/models/cats_detalle.dart';
import 'package:tudemo/models/product_cars.dart';
import 'package:tudemo/models/products.dart';
import 'package:tudemo/repository/blog_repository.dart';

part 'bloc_event.dart';
part 'bloc_state.dart';

class BlocBloc extends Bloc<BlocEvent, BlocState> {
  final BlogRepository _blogRepository;
  StreamSubscription _blogSubscription;

  BlocBloc({@required BlogRepository blogRepository})
      : assert(blogRepository != null),
        _blogRepository = blogRepository,
        super(null);

  @override
  BlocState get initialState => BlogLoading();

  @override
  Stream<BlocState> mapEventToState(
    BlocEvent event,
  ) async* {
    if (event is LoadBlog) {
      yield* _mapLoadBlogToState();
    } else if (event is Delete) {
      yield* _mapDeleteToState(event);
    } else if (event is AddProductCar) {
      yield* _mapAddProductCarToState(event);
    } else if (event is BlogUpdated) {
      yield* _mapBlogUpdatedToState(event);
    }
  }

  Stream<BlocState> _mapLoadBlogToState() async* {
    yield BlogLoading();
    _blogSubscription?.cancel();
    try {
      _blogSubscription = _blogRepository
          .getPostsProductCars()
          .listen((posts) => add(BlogUpdated(posts)));
    } catch (_) {
      yield BlogNoLoaded();
    }
  }

  Stream<BlocState> _mapDeleteToState(Delete event) async* {
    try {
      await _blogRepository.deleteProductCars(event.idDocument);
    } catch (_) {
      yield BlogNoLoaded();
    }
  }

  Stream<BlocState> _mapAddProductCarToState(AddProductCar productCar) async* {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var idCart = prefs.getInt("idCart");
    await _blogRepository.putPostProductCars(
        productCar.productid, idCart, productCar.quantity);
  }

  Stream<BlocState> _mapBlogUpdatedToState(BlogUpdated event) async* {
    final List<Products> products = await detallelis();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var idCart = prefs.getInt("idCart");
    var cantidad = await _blogRepository.getPostsProductCars().first;
    var cantidadnum =
        cantidad.where((element) => element.cart_id == idCart).length;
    yield BlogLoaded(products, cantidadnum);
  }

  Future<List<Products>> detallelis() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var idCart = prefs.getInt("idCart");
    var status = prefs.getBool("pendig");
    if (idCart == null|| status == false) {
      var rng = new Random().nextInt(100000);
      await _blogRepository.putPostCars(rng);
      List<Cart> result = await _blogRepository.getPostsCart().first;
      Cart id =result.where((element) => element.id==rng).first;
      prefs.setInt("idCart", id.id);
      prefs.setString("documentID", id.documentID);
      prefs.setBool("pendig", true);
    }
    final List<Products> products = await _blogRepository.getPosts().first;

    return products;
  }

  

  @override
  Future<void> close() {
    _blogSubscription?.cancel();
    return super.close();
  }
}
