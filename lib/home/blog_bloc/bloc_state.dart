part of 'bloc_bloc.dart';

abstract class BlocState extends Equatable {
  const BlocState();
  
  @override
  List<Object> get props => [];
}

class BlogLoading extends BlocState {
  @override
  String toString() => 'Blog Loading';
}

class BlogPendig extends BlocState {
  @override
  String toString() => 'Blog Loading';
}

class BlogCompleted extends BlocState {
  final bool posts;

  const BlogCompleted([this.posts]);

  @override
  List<Object> get props => [posts];

  @override
  String toString() => 'Blog loaded';
}

class BlogLoaded extends BlocState {
  final List<Products> posts;
  final int cart;

  const BlogLoaded([this.posts,this.cart]);

  @override
  List<Object> get props => [posts,cart];

  @override
  String toString() => 'Blog loaded';
}

class BlogNoLoaded extends BlocState {
  @override
  String toString() => 'Blog No Loaded';
}
