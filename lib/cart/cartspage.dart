import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tudemo/cart/blog_bloc/bloc_bloc.dart';
import 'package:tudemo/home/home.dart';
import 'package:tudemo/models/cats_detalle.dart';
import 'package:tudemo/repository/blog_repository.dart';

class CartsPage extends StatefulWidget {
  @override
  _CartsPageState createState() => _CartsPageState();
}

class _CartsPageState extends State<CartsPage> {
  List<CartDetalle> postList = [];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocBloc, BlocState>(builder: (context, state) {
      if (state is BlogLoading) {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
      if (state is BlogNoLoaded) {
        return Center(
          child: Column(
            children: <Widget>[Icon(Icons.error), Text('Cannot load posts')],
          ),
        );
      }
      if (state is BlogLoaded) {
        postList = state.posts;
        return Scaffold(
            body: Container(
                child: postList.length == 0
                    ? Center(
                        child: Text('No Blog Avaliable'),
                      )
                    : ListView.builder(
                        itemCount: postList.length,
                        itemBuilder: (_, index) {
                          return postsUI(
                            postList[index].nombre.toString(),
                            postList[index].id.toString(),
                            postList[index].quantity,
                          );
                        },
                      )),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
            floatingActionButton: Visibility(
              visible:postList.length == 0 ? false : true,
              child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: GestureDetector(
                    onTap: () async {
                      BlocBloc(blogRepository: BlogRepository())
                        ..add(BlogUpdatedCard());
                       


                      Navigator.of(context).pushNamedAndRemoveUntil(
                                  '/home',
                                  (Route<dynamic> route) => false);
                            


                    },
                    child: Container(
                      width: double.infinity,
                      height: 50,
                      color: Colors.red,
                      child: Align(
                          child: Text(
                        "CONFIRMAR",
                        style: TextStyle(fontSize: 30, color: Colors.white),
                      )),
                    ),
                  )),
            ));
      }
      return Container();
    });
  }

  Widget postsUI(String nombre, String idDocument, int quantity) {
    return Card(
      elevation: 10.0,
      margin: EdgeInsets.all(14.0),
      child: Container(
        padding: EdgeInsets.all(14.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: [
                    Text(
                      "Nombre",
                      style: Theme.of(context).textTheme.subtitle2,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      nombre,
                      style: Theme.of(context).textTheme.subtitle2,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      "Unidades",
                      style: Theme.of(context).textTheme.subtitle2,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      quantity.toString(),
                      style: Theme.of(context).textTheme.subtitle2,
                      textAlign: TextAlign.center,
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      BlocBloc(blogRepository: BlogRepository())
                        ..add(Delete(idDocument));
                    }),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    IconButton(
                        icon: Icon(Icons.remove),
                        onPressed: () {
                          if (quantity == 1) {
                            BlocBloc(blogRepository: BlogRepository())
                              ..add(Delete(idDocument));
                          } else {
                            BlocBloc(blogRepository: BlogRepository())
                              ..add(UpdatedProductCar(
                                  idDocument, (quantity - 1)));
                          }
                        }),
                    IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          BlocBloc(blogRepository: BlogRepository())
                            ..add(
                                UpdatedProductCar(idDocument, (quantity + 1)));
                        })
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
