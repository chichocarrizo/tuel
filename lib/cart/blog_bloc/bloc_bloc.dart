import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tudemo/models/cats_detalle.dart';
import 'package:tudemo/models/product_cars.dart';
import 'package:tudemo/models/products.dart';
import 'package:tudemo/repository/blog_repository.dart';

part 'bloc_event.dart';
part 'bloc_state.dart';

class BlocBloc extends Bloc<BlocEvent, BlocState> {
  final BlogRepository _blogRepository;
  StreamSubscription _blogSubscription;

  BlocBloc({@required BlogRepository blogRepository})
      : assert(blogRepository != null),
        _blogRepository = blogRepository,
        super(null);

  @override
  BlocState get initialState => BlogLoading();

  @override
  Stream<BlocState> mapEventToState(
    BlocEvent event,
  ) async* {
    if (event is LoadBlog) {
      yield* _mapLoadBlogToState();
    } else if (event is Delete) {
      yield* _mapDeleteToState(event);
    } else if (event is AddProductCar) {
      yield* _addProductCarToState(event);
    } else if (event is UpdatedProductCar) {
      yield* _mapUpdatedProductCarToState(event);
    } else if (event is BlogUpdated) {
      yield* _mapBlogUpdatedToState(event);
    } else if (event is BlogUpdatedCard) {
      yield* _mapBlogUpdatedCardToState(event);
    }
  }

  Stream<BlocState> _mapLoadBlogToState() async* {
    yield BlogLoading();
    _blogSubscription?.cancel();
    try {
      _blogSubscription = _blogRepository
          .getPostsProductCars()
          .listen((posts) => add(BlogUpdated(posts)));

      var listdetalle = await detallelis();
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var pendig = prefs.getBool("pendig");
      yield BlogLoaded(listdetalle, pendig);
    } catch (_) {
      yield BlogNoLoaded();
    }
  }

  Stream<BlocState> _mapDeleteToState(Delete event) async* {
    try {
      await _blogRepository.deleteProductCars(event.idDocument);
    } catch (_) {
      yield BlogNoLoaded();
    }
  }

  Stream<BlocState> _mapBlogUpdatedCardToState(BlogUpdatedCard event) async* {
    try {
       final SharedPreferences prefs = await SharedPreferences.getInstance();
       var documentID = prefs.getString("documentID");
       prefs.setBool("pendig", false);
       await _blogRepository.putCars(documentID);
       
    } catch (_) {
      yield BlogNoLoaded();
    }
  }

  Stream<BlocState> _addProductCarToState(AddProductCar event) async* {
    try {
      await _blogRepository.putPostProductCars(
          event.productid, event.cartid, event.quantity);
    } catch (_) {
      yield BlogNoLoaded();
    }
  }

  Stream<BlocState> _mapUpdatedProductCarToState(
      UpdatedProductCar productCar) async* {
    yield BlogLoading();
    try {
      await _blogRepository.putProductCars(
          productCar.quantity, productCar.idDocument);
    } catch (_) {
      yield BlogNoLoaded();
    }
  }

  Stream<BlocState> _mapBlogUpdatedToState(BlogUpdated event) async* {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var pendig = prefs.getBool("pendig");
    var detalles = await detallelis();
    yield BlogLoaded(detalles, pendig);
  }

  Future<List<CartDetalle>> detallelis() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var idCart = prefs.getInt("idCart");
    final List<Products> products = await _blogRepository.getPosts().first;
    final List<ProductCar> productsCart =
        await _blogRepository.getPostsProductCars().first;
    final cantidad = productsCart.where((element) => element.cart_id==idCart).length;
    if(cantidad == 0 )
    {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
       var documentID = prefs.getString("documentID");
       prefs.setBool("pendig", false);
       await _blogRepository.putCars(documentID);
    }    
    List<CartDetalle> detalle = new List<CartDetalle>();
    productsCart.forEach((element1) {
      if (element1.cart_id == idCart) {
        var producto = products
            .where((element) => element.id == element1.product_id)
            .first;
        if (producto != null) {
          CartDetalle nuevo = new CartDetalle(
              producto.nombre, element1.documentID, element1.quantity);
          detalle.add(nuevo);
        }
      }
    });
    return detalle;
  }

  @override
  Future<void> close() {
    _blogSubscription?.cancel();
    return super.close();
  }
}
