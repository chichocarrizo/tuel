part of 'bloc_bloc.dart';

abstract class BlocEvent extends Equatable {
  const BlocEvent();

  @override
  List<Object> get props => [];
}

class LoadBlog extends BlocEvent {}

class Delete extends BlocEvent {
  final String idDocument; 
  

 const Delete(this.idDocument);

  @override
  List<Object> get props => [idDocument];

  @override
  String toString() => 'Delete ProductCar';
}

class AddProductCar extends BlocEvent{
  final int productid; 
  final int cartid;
  final int quantity; 

 const AddProductCar(this.productid,this.cartid,this.quantity);

  @override
  List<Object> get props => [productid, cartid,quantity];

  @override
  String toString() => 'Adding ProductCar';
}

class UpdatedProductCar extends BlocEvent {
  final String idDocument;
  final int quantity; 
  

 const UpdatedProductCar(this.idDocument,this.quantity);

  @override
  List<Object> get props => [idDocument,quantity];

  @override
  String toString() => ' Updated ProductCar';
}

class BlogUpdated extends BlocEvent {
  final List<ProductCar> posts;

  const BlogUpdated(this.posts);

  @override
  List<Object> get props => [posts];
}




class BlogUpdatedCard extends BlocEvent {
  const BlogUpdatedCard();

  @override
  List<Object> get props => [];
}