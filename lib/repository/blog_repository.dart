import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tudemo/models/carts.dart';
import 'package:tudemo/models/product_cars.dart';
import 'package:tudemo/models/products.dart';

class BlogRepository {
  final databaseReference = Firestore.instance;

  Stream<List<Cart>> getPostsCart() {
    return databaseReference.collection("Carts").snapshots().map((snapshot) {
      return snapshot.documents.map((doc) => Cart.fromSnapshot(doc)).toList();
    });
  }

  Stream<List<ProductCar>> getPostsProductCars() {
    return databaseReference.collection("ProductsCars").snapshots().map((snapshot) {
      return  snapshot.documents.map((doc) => ProductCar.fromSnapshot(doc)).toList();
    });
   
  }


  Stream<List<Products>> getPosts() {
    return databaseReference.collection("Products").snapshots().map((snapshot) {
      return snapshot.documents.map((doc) => Products.fromSnapshot(doc)).toList();
    });
  }

  Future<void>putPostProductCars(int productid, int cartid, int quantity ){
    databaseReference.collection("ProductsCars").document().setData(
      {
        'product_id':productid,
         'cart_id':cartid,
          'quantity':quantity
      }
    );
    
  }

  Future<void>putPostCars(int id){
    databaseReference.collection("Carts").document().setData(
      {
         'id':id,
         'status':"pending",
      }
    );
    
  }

  Future<void>putProductCars( int quantity,String idDocumento) async{
    databaseReference.collection("ProductsCars").document(idDocumento).updateData(
      {
         'quantity':quantity
      }
    );
    
  }

  Future<void>putCars(String idDocumento) async{
    databaseReference.collection("Carts").document(idDocumento).updateData(
      {
         'status':"completed",
      }
    );
    
  }

  Future<void>deleteProductCars(String documentId ) async{
    databaseReference.collection("ProductsCars").document(documentId).delete();  
  }



  
}