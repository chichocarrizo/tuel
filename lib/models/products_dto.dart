
class ProductsDto{
  final String nombre, sku, descripcion;
  final int id;

  const ProductsDto(this.nombre, this.sku, this.descripcion, this.id);
}