import 'package:cloud_firestore/cloud_firestore.dart';

class Products{
  final String nombre, sku, descripcion;
  final int id;

  const Products(this.nombre, this.sku, this.descripcion, this.id);

  static Products fromSnapshot(DocumentSnapshot snapshot) {
        return Products(
          snapshot.data['nombre'],
          snapshot.data['sku'],
          snapshot.data['descripcion'],
          snapshot.data['id']
    );
  }
}