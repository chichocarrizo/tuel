import 'package:cloud_firestore/cloud_firestore.dart';

class Cart{
  final String  status , documentID;
  final int id;

  const Cart( this.status , this.id,this.documentID);

  static Cart fromSnapshot(DocumentSnapshot snapshot) {
        return Cart(
          snapshot.data['status'],
          snapshot.data['id'],
          snapshot.documentID
    );
  }
}