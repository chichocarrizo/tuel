import 'package:cloud_firestore/cloud_firestore.dart';

class ProductCar{
  final String documentID;
  final int product_id,cart_id,quantity;

  const ProductCar(this.product_id, this.cart_id, this.quantity,this.documentID);

  static ProductCar fromSnapshot(DocumentSnapshot snapshot) {
        return ProductCar(
          snapshot.data['product_id'],
          snapshot.data['cart_id'],
          snapshot.data['quantity'],
          snapshot.documentID
    );
  }
}