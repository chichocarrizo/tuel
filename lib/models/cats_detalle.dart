
class CartDetalle{
  final String  nombre,id ;
  final int quantity;

  const CartDetalle( this.nombre , this.id , this.quantity);

  String get get_nombre {
    return nombre;
  }
   
  set nombre (String name) {
    this.nombre = name;
  }

  String get get_id {
    return id;
  }
   
  set id (String id) {
    this.id = id;
  }
  int get get_quantity {
      return quantity;
    }
    
    set quantity (int quantity) {
      this.quantity = quantity;
    }

}